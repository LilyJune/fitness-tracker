from datetime import datetime
from app import DB

class User(DB.Model):
    '''The Class for the User Table in the DB'''
    id = DB.Column(DB.Integer, primary_key=True)
    username = DB.Column(DB.String(64), index=True, unique=True)
    email = DB.Column(DB.String(120), index=True, unique=True)
    password_hash = DB.Column(DB.String(128))
    weight_records = DB.relationship('WeightRecord', backref='user', lazy='dynamic')

    def __repr__(self):
        '''Outputs the username'''
        return '<User {}>'.format(self.username)

class WeightRecord(DB.Model):
    '''The Class for the Weight records table in the db'''
    id = DB.Column(DB.Integer, primary_key=True)
    timestamp = DB.Column(DB.DateTime, nullable=False, index=True, default=datetime.utcnow)
    user_id = DB.Column(DB.Integer, DB.ForeignKey('user.id'))
    weight = DB.Column(DB.Float, nullable=False)

    def __repr__(self):
        '''Outputs the weight record'''
        return '<WeightRecord {}>'.format(self.user_id)
