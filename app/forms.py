from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, DecimalField
from wtforms.validators import DataRequired

class LoginForm(FlaskForm):
    '''The login form generator'''
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')

class WeightLoggingForm(FlaskForm):
    '''the form for logging your weight'''
    weight = DecimalField(places=1, validators=[DataRequired()])
    submit = SubmitField('Submit')
