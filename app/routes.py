from flask import render_template, flash, redirect, url_for
from app import APP, DB
from app.forms import LoginForm, WeightLoggingForm
from app.graphs import HistoryGraph
from app.models import User, WeightRecord

@APP.route('/')
@APP.route('/index')
def index():
    '''Renders the Index'''
    user = {'username': 'Lily'}
    return render_template('index.html', title='Home', user=user)

@APP.route('/login', methods=['GET', 'POST'])
def login():
    '''Renders the Login page'''
    form = LoginForm()
    if form.validate_on_submit():
        flash('login requested for user {}, remember_me={}'.format(
            form.username.data, form.remember_me.data))
        return redirect(url_for('index'))
    return render_template('login.html', title='Sign In', form=form)

@APP.route('/report', methods=['GET'])
def report():
    '''Renders the report with a graph'''
    history = HistoryGraph()
    return history.graph.to_html()

@APP.route('/log', methods=['GET', 'POST'])
def log():
    '''Allows a user to log their weight'''
    log_form = WeightLoggingForm()
    if log_form.validate_on_submit():
        user = User.query.get(1)
        weight = log_form.weight.data
        weight_record = WeightRecord(user_id=user.id, weight=weight)

        DB.session.add(weight_record)
        DB.session.commit()

        return redirect(url_for('report'))
    return render_template('weight-logger.html', title='Weight Logging', form=log_form)
