import altair as alt
import pandas as pd
from app.models import User

class HistoryGraph():
    '''A Graph of the history of the person's weight'''

    user = User.query.get(1)
    user_weight_records = user.weight_records.all()
    d = []

    for wr in user_weight_records:
        d.append({'date': wr.timestamp, 'weight': wr.weight})

    print(d)

    df = pd.DataFrame(data=d)

    graph = alt.Chart(df).mark_line().encode(
        x='date:T',
        y='weight:Q',
    )
